typedef struct {
  int up_pressed;
  int down_pressed;
  int left_pressed;
  int right_pressed;
} button_struct;

void button_gpio_setup(void);

void button_more_gpio(void);

void setLEDs(int v);

button_struct clear_buttons(button_struct buttons);

button_struct button_setup(void);
button_struct fake_button_setup(void);

button_struct read_buttons(button_struct buttons);
//int read_buttons(button_struct buttons);



