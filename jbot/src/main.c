#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>

#include "serial.h"
#include "serial_rb.h"
#include "nrf24l01.h"
#include "conio.h"

#include "button.h"
#include "nrf2serial.h"
#include "l298n.h"

button_struct process_buttons (nrf_payload p, button_struct buttons);
button_struct read_char (button_struct buttons);

void send_byte(nrf_payload p, char c) {
      p.size = 8;
      p.data[0] = 1;
      p.data[1] = c;
      nrf_send_blocking(&p);
}

button_struct process_buttons (nrf_payload p, button_struct buttons) {
	buttons = read_buttons (buttons);
	if (buttons.up_pressed) {
	    send_byte(p, 'A');
	    setLEDs(1);
	}
	else if (buttons.down_pressed) {
	    send_byte(p, 'B');
	    setLEDs(2);
	}
	else if (buttons.left_pressed) {
	    send_byte(p, 'C');
	    setLEDs(3);
	}
	else if (buttons.right_pressed) {
	    send_byte(p, 'D');
	    setLEDs(4);
	}
	else {
	    send_byte(p, 'E');
	    setLEDs(4);
	}
	return buttons;
}

button_struct read_char (button_struct buttons) {
/*
        if(!serial_rb_empty(&stx)) {
		char c = serial_rb_read(&stx);
		if (c == 'A') {
			buttons.up_pressed = 1;
		}
		else { 
			buttons.down_pressed = 1;
		}
        }
	else { 
		buttons.up_pressed = 0;
		buttons.down_pressed = 0;
	}
*/
	return buttons;
}
void nrf_configure_sb_rx(void);
void nrf_configure_sb_tx(void);
void test_receive(void);
void test_send(void);

void nrf_configure_sb_rx(void) {

	// Set address for TX and receive on P0
 	nrf_reg_buf addr;

	addr.data[0] = 1;
	addr.data[1] = 2;
	addr.data[2] = 3;
	addr.data[3] = 4;
	addr.data[4] = 5;

	nrf_preset_sb(NRF_MODE_PRX, 40, 1, &addr);

	// Wait for radio to power up 
	delay(10000 * 1000);
}

void nrf_configure_sb_tx(void) {

	// Set address for TX and receive on P0
 	nrf_reg_buf addr;

	addr.data[0] = 1;
	addr.data[1] = 2;
	addr.data[2] = 3;
	addr.data[3] = 4;
	addr.data[4] = 5;

	nrf_preset_sb(NRF_MODE_PTX, 40, 1, &addr);

	// Wait for radio to power up
	delay(100000 * 10);
}

void test_send(void) {
  int i = 0;
  nrf_payload p;

  nrf_set_mode_ptx();

  while (1) {
    i++;
    if (i < 20) {
      p.size = 8;
      p.data[0] = 1;
      p.data[1] = 'C';
      nrf_send_blocking(&p);
    }
    else if (i < 100000) { __asm__("nop"); 
    } else if (i < 100020) { 
//      __asm__("nop");
      p.size = 8;
      p.data[0] = 1;
      p.data[1] = 'A';
      nrf_send_blocking(&p);
    }
    else if (i < 3000000) {
      __asm__("nop");
    }
    else if (i < 3000020) { 
//      __asm__("nop");
      p.size = 8;
      p.data[0] = 1;
      p.data[1] = 'B';
      nrf_send_blocking(&p);
    }
    else if (i < 4000000) { i = 0; }
  }
}

void test_receive(void) {
  nrf_payload p;

  nrf_set_mode_prx();

  while (1) {
//    if (nrf_receive(&p) != 0) {
    nrf_receive(&p);
    if (p.data[1] == 'A') {
      setLEDs(1);
    }
    else if (p.data[1] == 'B') {
      setLEDs(2);
    }
    else if (p.data[1] == 'C') {
      setLEDs(3);
    }
    else {setLEDs(4);}
    p.data[1]='a';
//    buttons = read_char(buttons);
//    do_l298n(buttons);
  }
}

void receive_mode(void) {
  nrf_payload p;

  nrf_set_mode_prx();
  l298n_setup();
  button_struct buttons = fake_button_setup();
  int i = 0;

  while (1) {
    i++;
    if (i > 20) buttons = clear_buttons(buttons);
    nrf_receive(&p);
    if (p.data[1] == 'A') {
      buttons.up_pressed = 1;
    }
    else if (p.data[1] == 'B') {
      buttons.down_pressed = 1;
    }
    else if (p.data[1] == 'C') {
      buttons.left_pressed = 1;
    }
    else if (p.data[1] == 'D') {
      buttons.right_pressed = 1;
    }
    p.data[1]='a';

    do_l298n(buttons);
  }
}

void send_mode(void) {
  nrf_payload p;

  nrf_set_mode_ptx();

  button_struct buttons = button_setup();

  while (1) {
      process_buttons(p, buttons);
  }
}


int main (void) {
  nrf2serial_setup();


  #ifdef TRANSMITTER
    send_mode();
  #else
    receive_mode();
  #endif

  return (0);
}

