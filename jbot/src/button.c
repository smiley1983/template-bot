#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/timer.h>
#include <stddef.h>
#include <stdlib.h>
#include "button.h"

/* Set STM32 to 168 MHz. */
//static void clock_setup(void)
//{
//    rcc_clock_setup_in_hse_8mhz_out_72mhz();
//    rcc_clock_setup_hse_3v3(&hse_8mhz_3v3[CLOCK_3V3_50MHZ]);
//}

void button_gpio_setup(void)
{
    /* Enable GPIOB clock. */
    rcc_peripheral_enable_clock(&RCC_APB2ENR, RCC_APB2ENR_IOPBEN);

    /* Set GPIO 10 - 11 (in GPIO port B) to 'output push-pull'. */
    gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ,
            GPIO_CNF_OUTPUT_PUSHPULL, GPIO11 | GPIO10 | GPIO9);
}

void button_more_gpio(void)
{
    /* Enable GPIOA clock. */
    rcc_peripheral_enable_clock(&RCC_APB2ENR, RCC_APB2ENR_IOPAEN);

    /* Set GPIO7-9 (in GPIO port A) to input pullup. */
    gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO7 | GPIO6 | GPIO8 | GPIO9);
    gpio_set (GPIOA, GPIO7 | GPIO6 | GPIO8 | GPIO9);
}

void setLEDs(int v)
{

    if(v == 1){
        gpio_set(GPIOB, GPIO10);
        gpio_clear(GPIOB, GPIO11);
        return;
    }
    if(v == 2){
        gpio_set(GPIOB, GPIO11);
        gpio_clear(GPIOB, GPIO10);
        return;
    }
    if(v == 3){
        gpio_set(GPIOB, GPIO11);
        gpio_set(GPIOB, GPIO10);
        return;
    }
    if(v == 4){
        gpio_clear(GPIOB, GPIO10);
        gpio_clear(GPIOB, GPIO11);
        return;
    }
    if(v == 5){
        gpio_set(GPIOB, GPIO9);
        return;
    }
}

button_struct button_setup(void) {
	button_gpio_setup();
	button_more_gpio();

	button_struct buttons;
	buttons.up_pressed = 0;
	buttons.down_pressed = 0;
	buttons.left_pressed = 0;
	buttons.right_pressed = 0;
	return buttons;
}

button_struct fake_button_setup(void) {
	button_struct buttons;
	buttons.up_pressed = 0;
	buttons.down_pressed = 0;
	buttons.left_pressed = 0;
	buttons.right_pressed = 0;
	return buttons;
}

button_struct clear_buttons(button_struct buttons) {
	buttons.up_pressed = 0;
	buttons.down_pressed = 0;
	buttons.left_pressed = 0;
	buttons.right_pressed = 0;
	return buttons;
}

button_struct read_buttons(button_struct buttons)
{

//  gpio_setup();
//  clock_setup();
//  button_setup(); 
//  int i;

  if (!(GPIOA_IDR & (1 << 0x6))){
    buttons.up_pressed = 1;
//       for (i = 0; i < 3000000; i++)  /* Wait a bit. */
//                  __asm__("nop");
  }
  else { buttons.up_pressed = 0; }


  if (!(GPIOA_IDR & (1 << 0x7))){
    buttons.down_pressed = 1;
  }
  else { buttons.down_pressed = 0; }


  if (!(GPIOA_IDR & (1 << 0x8))){
    buttons.left_pressed = 1;
  }
  else { buttons.left_pressed = 0; }


  if (!(GPIOA_IDR & (1 << 0x9))){
    buttons.right_pressed = 1;
  }
  else { buttons.right_pressed = 0; }


  return buttons;

}

