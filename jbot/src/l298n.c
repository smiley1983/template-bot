/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2010 Piotr Esden-Tempski <piotr@esden.net>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include "nrf24l01.h"
#include "button.h"
//#include "nrf2serial.h"

#define SPEED_A TIM3_CCR3
#define SPEED_B TIM3_CCR4

int do_l298n (button_struct buttons);
void delay(uint32_t v);
void l298n_setup (void);

static void enable_A1 (void) {gpio_set(GPIOA, GPIO4);}
static void enable_A2 (void) {gpio_set(GPIOA, GPIO5);}
static void disable_A1 (void) {gpio_clear(GPIOA, GPIO4);}
static void disable_A2 (void) {gpio_clear(GPIOA, GPIO5);}

static void enable_B2 (void) {gpio_set(GPIOA, GPIO2);}
static void enable_B1 (void) {gpio_set(GPIOA, GPIO3);}
static void disable_B2 (void) {gpio_clear(GPIOA, GPIO2);}
static void disable_B1 (void) {gpio_clear(GPIOA, GPIO3);}

static void forward_A(void) {
	disable_A2();
	enable_A1();
}

static void reverse_A(void) {
	disable_A1();
	enable_A2();
}

static void stop_A(void) {
	SPEED_A = 0xffff;
	disable_A2();
	enable_A1();
}

static void forward_B(void) {
	disable_B2();
	enable_B1();
}

static void reverse_B(void) {
	disable_B1();
	enable_B2();
}

static void stop_B(void) {
	SPEED_B = 0xffff;
	disable_B2();
	enable_B1();
}

static void clock_setup(void)
{
	rcc_clock_setup_in_hse_8mhz_out_72mhz();

	// Enable TIM3 clock.
	rcc_periph_clock_enable(RCC_TIM3);

	// Enable GPIOC, Alternate Function clocks.
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_AFIO);
}

static void gpio_setup(void)
{
	/* Set GPIO4 and 5 (in GPIO port A) to 'output push-pull'. */
	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_PUSHPULL, GPIO4 | GPIO5 | GPIO2 | GPIO3);

	// Set GPIO6 and 7 (in GPIO port A) to
	// 'output alternate function push-pull'.
//	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
//		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
//		      GPIO_TIM3_CH1 | GPIO_TIM3_CH2);

	// Set GPIO0 and 1 (in GPIO port B) to
	// 'output alternate function push-pull'.
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
		      GPIO_TIM3_CH3 | GPIO_TIM3_CH4);

}

static void tim_setup(void)
{
	// Clock division and mode
	TIM3_CR1 = TIM_CR1_CKD_CK_INT | TIM_CR1_CMS_EDGE;
	// Period
	TIM3_ARR = 65535;
	// Prescaler
	TIM3_PSC = 0;
	TIM3_EGR = TIM_EGR_UG;

	// ----
	// Output compare 1 mode and preload
	TIM3_CCMR1 |= TIM_CCMR1_OC1M_PWM1 | TIM_CCMR1_OC1PE;

	// Polarity and state
	TIM3_CCER |= TIM_CCER_CC1P | TIM_CCER_CC1E;
	//TIM3_CCER |= TIM_CCER_CC1E;

	// Capture compare value
	TIM3_CCR1 = 0;

	// ----
	// Output compare 2 mode and preload
	TIM3_CCMR1 |= TIM_CCMR1_OC2M_PWM1 | TIM_CCMR1_OC2PE;

	// Polarity and state
	TIM3_CCER |= TIM_CCER_CC2P | TIM_CCER_CC2E;
	//TIM3_CCER |= TIM_CCER_CC2E;

	// Capture compare value
	TIM3_CCR2 = 0;

	// ----
	// Output compare 3 mode and preload
	TIM3_CCMR2 |= TIM_CCMR2_OC3M_PWM1 | TIM_CCMR2_OC3PE;

	// Polarity and state
	TIM3_CCER |= TIM_CCER_CC3P | TIM_CCER_CC3E;
	//TIM3_CCER |= TIM_CCER_CC3E;

	// Capture compare value
	TIM3_CCR3 = 0;

	// ----
	// Output compare 4 mode and preload
	TIM3_CCMR2 |= TIM_CCMR2_OC4M_PWM1 | TIM_CCMR2_OC4PE;

	// Polarity and state
	TIM3_CCER |= TIM_CCER_CC4P | TIM_CCER_CC4E;
	//TIM3_CCER |= TIM_CCER_CC4E;

	// Capture compare value
	TIM3_CCR4 = 0;

	// ----
	// ARR reload enable
	TIM3_CR1 |= TIM_CR1_ARPE;

	// Counter enable
	TIM3_CR1 |= TIM_CR1_CEN;
}

void delay(uint32_t v) {
        uint16_t i;
        for (; v > 0; v--) { for (i = 10; i>0; i--){__asm("nop");} }
}


void l298n_setup (void) {

	clock_setup();
	gpio_setup();
	tim_setup();
	stop_A();
	stop_B();

}

void set_forward_A (int speed) {
	SPEED_A = 0xffff - speed;
	forward_A();
}

void set_reverse_A (int speed) {
	SPEED_A = 0xffff - speed;
	reverse_A();
}

int do_l298n (button_struct buttons)
{

	if (buttons.up_pressed) {
		SPEED_A = 0;
		forward_A();
		SPEED_B = 0;
		forward_B();
	}
	else if (buttons.down_pressed) {
		SPEED_A = 0;
		reverse_A();
		SPEED_B = 0;
		reverse_B();
	}
	else if (buttons.left_pressed) {
		SPEED_A = 0;
		forward_A();
		SPEED_B = 0;
		reverse_B();
	}
	else if (buttons.right_pressed) {
		SPEED_A = 0;
		reverse_A();
		SPEED_B = 0;
		forward_B();
	}
	else {
		stop_A();
		stop_B();
	}
		delay(0xff);

	return 0;
}

/*
int do_l298n (button_struct buttons)
{

	if (buttons.up_pressed) {
		SPEED_A = 0;
		forward_A();
		delay(0xfff);
	}
	else if (buttons.down_pressed) {
		SPEED_A = 0;
		reverse_A();
		delay(0xfff);
	}
	else { stop_A(); }
	if (buttons.left_pressed) {
		SPEED_B = 0;
		forward_B();
		delay(0xfff);
	}
	else if (buttons.right_pressed) {
		SPEED_B = 0;
		reverse_B();
		delay(0xfff);
	}
	else {
		stop_B();
	}

	return 0;
}
*/

